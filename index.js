const express = require("express");
const app = express();
const PORT = 4000;
const excellJs = require("exceljs");

var fs = require("fs");

app.get("/", function (req, res) {
  res.send("Hello World");
});

app.get("/export", async (req, res) => {
  try {
    let workBook = new excellJs.Workbook();
    const sheet = workBook.addWorksheet("books");
    sheet.columns = [
      {
        header: "ISBN",
        key: "isbn",
        width: "25",
      },
      {
        header: "Title",
        key: "title",
        width: "50",
      },
      {
        header: "Author",
        key: "author",
        width: "50",
      },
      {
        header: "Page Count",
        key: "pages",
        width: "10",
      },
    ];

    let object = JSON.parse(fs.readFileSync("data.json", "utf8"));
    await object.books.map((value, idx) => {
      sheet.addRow({
        isbn: value.isbn,
        title: value.title,
        author: value.author,
        pages: value.pages,
      });
    });
    res.setHeader(
      "Content-Type",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );
    res.setHeader("Content-Disposition", "attachment;filename=" + "books.xlsx");
    workBook.xlsx.write(res);
  } catch (error) {
    console.log(error);
  }

  // const workbook = createAndFillWorkbook();
  // await workbook.xlsx.writeFile("soKho.tonghop_template.xlsx");

  // const workbook = new excellJs.Workbook();
  // await workbook.xlsx.readFile("soKho.tonghop_template.xlsx");
});

app.listen(PORT, () => {
  console.log(`Server running on Port: ${PORT}`);
});
